﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIBFinal
{
    public partial class dashboard : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            Total();
            Todays();
            Advance();
            Activevisitors();
        }





        private void Total()
        {


            SqlConnection conn = new SqlConnection(connstrg);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SELECT COUNT(Id) as Counting FROM [Visitors] where status1='1'", conn);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                lbltotal.Text = dr["Counting"].ToString();


            }
            else
            {
                lbltotal.Text = "0";
            }
            dr.Close();
            conn.Close();
        }
        protected void Activevisitors()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) as Counting FROM [dbo].[Visitors] where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status1='1' and  outtime is NULL ", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblactivevisitor.Text = dr["Counting"].ToString();
            }
            else
            {
                lblactivevisitor.Text = "0";
            }
            dr.Close();
            conn.Close();
        }
        private void Todays()
        {


            SqlConnection conn = new SqlConnection(connstrg);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SELECT COUNT(Id) as Counting FROM [Visitors] where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status1='1' ", conn);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                lbltodaylvis.Text = dr["Counting"].ToString();


            }
            else
            {
                lbltodaylvis.Text = "0";
            }
            dr.Close();
            conn.Close();
        }

        protected void Advance()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(Id) as Counting FROM [Visitors] where status1='0' ", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lbladavncelist.Text = dr["Counting"].ToString();
            }
            else
            {
                lbladavncelist.Text = "0";
            }
            dr.Close();
            conn.Close();
        }

    }
}