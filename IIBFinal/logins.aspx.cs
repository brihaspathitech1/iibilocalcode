﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IIBI.DataAccess;

namespace IIBFinal
{
    public partial class logins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loginsss();
        }


        private void loginsss()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  AdminLogin where role='"+Session["role"].ToString()+"'", conn44);

            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();




            nae.Text = gRow.Cells[0].Text.Replace("&nbsp;", "");
            userna.Text = gRow.Cells[1].Text.Replace("&nbsp;", "");
            passw.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");

            this.ModalPopupExtender2.Show();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString);

            conn44.Open();


            SqlCommand cmd = new SqlCommand("update AdminLogin set name='" + nae.Text + "',username='" + userna.Text + "',password='" + passw.Text + "' where id='" + lblstor_id.Text + "'", conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            loginsss();
        }
    }
}