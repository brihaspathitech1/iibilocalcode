﻿using IIBI.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIBFinal
{
    public partial class AddVisitor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        protected void BindStudy()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT Distinct dept FROM tblEmployee order by dept";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList2.DataSource = dt;

                        DropDownList2.DataTextField = "dept";
                        DropDownList2.DataBind();
                        sqlConn.Close();
                        DropDownList2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Department", "0"));
                    }
                }
            }
            catch { }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT id,name FROM tblemployee where dept='" + DropDownList2.SelectedItem.Text + "' ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList1.DataSource = dt;
                        DropDownList1.DataValueField = "id";
                        DropDownList1.DataTextField = "name";

                        DropDownList1.DataBind();
                        sqlConn.Close();
                        DropDownList1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Employee", "0"));
                        
                    }
                }
            }
            catch
            {

            }

        }

        
        private double newvid()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(max(vid),0) from Visitors");
            double vid = 100;
            if (Convert.ToString(ds.Tables[0].Rows[0][0]) != "")
            {
                vid = double.Parse(ds.Tables[0].Rows[0][0].ToString());
                vid = vid + 1;
            }
            return vid;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                double vid = newvid();



                DataSet dsfloor = DataQueries.SelectCommon("select FLOOR from tblemployee where dept='" + DropDownList2.SelectedItem.Text.Trim() + "'");

                lblfloor.Text = dsfloor.Tables[0].Rows[0][0].ToString();


                string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
                SqlConnection conn1 = new SqlConnection(connstrg);
                String insert = "insert into Visitors(Name,Mobile,Email,Address,Photo,Purpose,date,Department,WhoomToMeet,Company,vid,statusby,asset,status,NoofPersons,floor,status1)values('" + txtvisitor.Text + "','" + txtmbl.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','/Image/" + vid + ".jpg','" + Purpose.Text + "','" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "','" + DropDownList2.SelectedItem.Text + "','" + DropDownList1.SelectedItem.Text + "','" + txtcompany.Text + "','" + vid + "','" + Session["id"].ToString() + "','" + txtassets.Text + "','arrive','" + txttotalvisitor.Text + "','" + lblfloor.Text + "','1')";
                SqlCommand comm1 = new SqlCommand(insert, conn1);

                conn1.Open();
                comm1.ExecuteNonQuery();
                conn1.Close();
                clear();
                Response.Redirect("WebForm1.aspx?id=" + vid);
            }
            catch (Exception ex)

            {
                throw ex;
                //Response.Redirect("default.aspx?");

            }
        }

        private void clear()
        {
            txtvisitor.Text = "";
            txtmbl.Text = "";
            txtemail.Text = "";
            txtaddress.Text = "";
            DropDownList2.SelectedIndex = -1;
            DropDownList1.SelectedIndex = -1;
            //WhoomToMeet.Text = "";
            Purpose.Text = "";
            txtdate.Text = "";

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}