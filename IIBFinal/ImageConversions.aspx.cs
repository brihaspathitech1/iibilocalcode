﻿using IIBI.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIBFinal
{
    public partial class ImageConversions : System.Web.UI.Page
    {
        private double newvid()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(max(vid),0) from Visitors");
            double vid = 1;
            if (Convert.ToString(ds.Tables[0].Rows[0][0]) != "")
            {
                vid = double.Parse(ds.Tables[0].Rows[0][0].ToString());
                vid = vid + 1;
            }
            return vid;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            double vid = newvid();
            string strPhoto = Request.Form["imageData"]; //obter imagem do flash
            byte[] photo = Convert.FromBase64String(strPhoto);
            //FileStream fs = new FileStream("F:/Uday Projects/Andhra Bank/WEBCAMERAAPP mine/WEBCAMERAAPP/Image/"+vid+".jpg", FileMode.OpenOrCreate, FileAccess.Write);
            FileStream fs = new FileStream(@"C:\IIBI\IIBI\Image\" + vid + ".jpg", FileMode.OpenOrCreate, FileAccess.Write);
            // FileStream fs = new FileStream(@"F:\Mahesh Projects\CLOUD PROJECTS\WEBCAMERAAPP222019\WEBCAMERAAPP\Image\" + vid + ".jpg", FileMode.OpenOrCreate, FileAccess.Write);

            BinaryWriter br = new BinaryWriter(fs);
            br.Write(photo);
            br.Flush();
            br.Close();
            fs.Close();
        }
    }
}