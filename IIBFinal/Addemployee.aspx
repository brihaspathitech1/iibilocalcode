﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="Addemployee.aspx.cs" Inherits="IIBFinal.Addemployee" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }

        body {
            color: #000 !important;
            font-weight: bold !important;
        }
    </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <section class="content-header">
                <h4>Add Employee Details</h4>
                <hr /></section>

    <section class="content">
	<div class="row">

         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
             <h3>&nbsp</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
                <div class="box-body">
              <div class="row">
                 
                  <div class="col-md-4">
                <div class="form-group">
                            Employee Id
                            <span class="style1">*</span>
                            <asp:TextBox ID="TextBox1" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                ErrorMessage="Enter id" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            
                        </div>
                      </div>
                      
                        <div class="col-md-4">
                <div class="form-group">
                            Employee Name
                            <span class="style1">*</span>
                            <asp:TextBox ID="Name" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name"
                                ErrorMessage="Enter Name" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            
                        </div></div>
                       <div class="col-md-4">
                <div class="form-group">
                            Department
                            <span class="style1">*</span>
                            <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox2"
                                ErrorMessage="Enter Department" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            
                        </div>
                        </div>
                   <div class="col-md-4">
                <div class="form-group">
                            Mobile
                            
                            <asp:TextBox ID="Mobile" class="form-control" runat="server" ></asp:TextBox>
                           
                        </div></div>

                  <div class="col-md-4">
                <div class="form-group">
                        
                           Designation

 <asp:TextBox ID="Designation" class="form-control" runat="server"></asp:TextBox>


                        </div></div>
                    <div class="col-sm-4" style="margin-left: 11px; margin-top: 14px">
                            <asp:Button ID="Add" runat="server" Text="Add" class="btn btn-success btn-block" OnClick="Add_Click" />
                        </div>

                        </div></div>
<script>
                // This example displays an address form, using the autocomplete feature
                // of the Google Places API to help users fill in the information.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        { types: ['geocode'] });

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();

                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOSxb5hyc4b2X7YF9WZJVDVjsKhrEd1lI&libraries=places&callback=initAutocomplete"
                async defer></script>

        </section>
    </section>
</asp:Content>
