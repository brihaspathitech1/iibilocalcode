﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IIBI.DataAccess;

namespace IIBFinal
{
    public partial class VisitorsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Visitors();
            }
        }
        protected string GetUrl(string imagepath)
        {
            string[] splits = Request.Url.AbsoluteUri.Split('/');
            if (splits.Length >= 2)
            {
                string url = splits[0] + "//";
                for (int i = 2; i < splits.Length - 1; i++)
                {
                    url += splits[i];
                    url += "/";
                }
                return url + imagepath;
            }
            return imagepath;
        }
        private void Visitors()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Visitors where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status1='1' order by id  desc", conn44);

            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

        protected void btnToday_Click(object sender, EventArgs e)
        {
            string date = System.DateTime.Now.ToString();
            DataSet ds = DataQueries.SelectCommon("select * from Visitors where status1='1' order by id desc");
            GridView1.DataSource = ds;
            GridView1.DataBind();
        }

        protected void btnsignout_Click(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Visitors where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status1='1' and  outtime is NULL order by id desc", conn44);
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = "".Trim();
            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();

                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }


        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {

        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
        }

        protected void btnDetails_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            string id = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            Response.Redirect("VisitorDetails.aspx?id=" + id);
        }

        protected void btnBadge_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            string id = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("Select vid from Visitors where Id='" + id + "'");

            Response.Redirect("WebForm1.aspx?id=" + ds.Tables[0].Rows[0][0].ToString());
        }

        protected void lnkupdate_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            //Label2.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");
            // ModalPopupExtender2.Show();
            DataSet ds = DataQueries.SelectCommon("select date from Visitors where Id='" + Label1.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Update = "update [Visitors] set Status='Sign Off' where Id='" + Label1.Text + "'";
            SqlCommand comm = new SqlCommand(Update, conn);
            String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("VisitorsList.aspx");


        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
        //    DataSet ds = DataQueries.SelectCommon("select date from Visitors where Id='" + Label1.Text + "'");
        //    string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
        //    SqlConnection conn = new SqlConnection(connstrg);
        //    String Update = "update [Visitors] set Status='Sign Off' where Id='" + Label1.Text + "'";
        //    SqlCommand comm = new SqlCommand(Update, conn);
        //    String Update1 = "update [Visitors] set outtime='" + txtupdate.Text + "' where Id='" + Label1.Text + "'";
        //    SqlCommand comm1 = new SqlCommand(Update1, conn);
        //    conn.Open();
        //    comm.ExecuteNonQuery();
        //    comm1.ExecuteNonQuery();
        //    conn.Close();
        //    Response.Redirect("VisitorsList.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            {
                if (ddstatus.SelectedItem.Value == "Select Status")
                {
                    Response.Write("<script>alert('Please..! Select the Status')</script>");
                    return;
                }


                else
                {
                    string signout = "";
                    string waitingTime = "";

                    if (ddstatus.SelectedItem.Value == "Sign Off")
                    {
                        DataSet ds = DataQueries.SelectCommon("select date from Visitors where Id='" + Label1.Text + "'");
                        DateTime signin = DateTime.Parse(ds.Tables[0].Rows[0][0].ToString());
                        waitingTime = System.DateTime.Now.Subtract(signin).ToString();
                        signout = System.DateTime.Now.ToString();
                    }
                    else
                    {
                        waitingTime = "";

                        signout = "";
                    }
                    string status = ddstatus.SelectedItem.Text;
                    string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
                    SqlConnection conn = new SqlConnection(connstrg);
                    String Update = "update [Visitors] set Status='" + status + "',statusreason='" + txtReason.Text + "',statusby='" + Session["id"].ToString() + "',signout='" + signout + "',waittime='" + waitingTime + "' where Id='" + Label1.Text + "'";
                    SqlCommand comm = new SqlCommand(Update, conn);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("VisitorsList.aspx");
                }
            }
        }
    }
}