﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="IIBFinal.dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
    Dashboard
        
      </h1>
  
    </section>

    <!-- Main content -->
    <section class="content">
	
	
	<div class="row">
	
    	<div class="col-md-3 col-sm-6 col-xs-12">
            <a href="totalvisitors.aspx">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-group"></i></span>

            <div class="info-box-content">
          
                 <h3>Total Visitors</h3>
                 <h3 Text="0" class="count text-center"> <asp:Label ID="lbltotal" runat="server"></asp:Label></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
                </a>
          <!-- /.info-box -->
        </div>
		
         	<div class="col-md-3 col-sm-6 col-xs-12">
                 <a href="Activevisitorslist.aspx">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-group"></i></span>

            <div class="info-box-content">
          
                 <h3>Active Visitors</h3>
                 <h3 Text="0" class="count text-center"> <asp:Label ID="lblactivevisitor" runat="server"></asp:Label></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
                     </a>
          <!-- /.info-box -->
        </div>
        	<div class="col-md-3 col-sm-6 col-xs-12">
                <a href="Todayvisitors.aspx">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-group"></i></span>

            <div class="info-box-content">
          
                 <h3>Today Visitors</h3>
                 <h3 Text="0" class="count text-center"> <asp:Label ID="lbltodaylvis" runat="server"></asp:Label></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
                    </a>
          <!-- /.info-box -->
        </div>
        

        	<div class="col-md-3 col-sm-6 col-xs-12">
                <a href="UserList.aspx">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-group"></i></span>

            <div class="info-box-content">
          
                 <h3>Advance List</h3>
                 <h3 Text="0" class="count text-center"> <asp:Label ID="lbladavncelist" runat="server"></asp:Label></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
                    </a>
          <!-- /.info-box -->
        </div>
        	
        
		
	 
	</div>


        <%--<div class="row">
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Heading</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                
              
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
             <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Heading</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                
              
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>--%>

        </section>
   
	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>

</asp:Content>
