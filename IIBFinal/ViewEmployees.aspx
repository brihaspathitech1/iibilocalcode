﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="ViewEmployees.aspx.cs" Inherits="IIBFinal.ViewEmployees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
      <h1>Employees List</h1></section>
    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

         <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
            <%--  <h3>&nbsp</h3>--%>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
               
 <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
           
          <%--  <asp:ImageField DataImageUrlField="Photo" HeaderText="Picture">
            </asp:ImageField>--%>

           
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>

            <asp:BoundField DataField="designation" HeaderText="Designation" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="dept" HeaderText="Department" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
             <asp:TemplateField  HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
              <asp:LinkButton ID="lnkupdate" OnClick="lnkupdate_Click"  runat="server" Text="Edit" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField  HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
              <asp:LinkButton ID="lnkDelete" OnClick="lnkDelete_Click"  runat="server" Text="Delete" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
            </Columns>
    </asp:GridView>
                      <asp:Button ID="Button5" runat="server" style="display:none" />
   <cc1:ModalPopupExtender  ID="ModalPopupExtender1" runat="server" TargetControlID="Button5" PopupControlID="Panel1"
CancelControlID="Button3" BackgroundCssClass="tableBackground"></cc1:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" BackColor="white" Style="display: none; border: 1px solid #ceceec;
        padding-bottom: 20px">
        <div class="col-md-12">
            <h3 class="text-info">
                Update </h3>
            <hr />
             <div class="box-body">
              <div class="row">
                 
                  <div class="col-md-4">
                <div class="form-group">
                            Employee Id
                            <span class="style1">*</span>
                            <asp:TextBox ID="txtempid" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                          
                            
                        </div>
                      </div>
                      
                        <div class="col-md-4">
                <div class="form-group">
                            Employee Name
                            <span class="style1">*</span>
                            <asp:TextBox ID="txtempname" class="form-control" runat="server"></asp:TextBox>
                            
                            
                        </div></div>
                       <div class="col-md-4">
                <div class="form-group">
                            Department
                            <span class="style1">*</span>
                            <asp:TextBox ID="txtdept" class="form-control" runat="server"></asp:TextBox>
                       
                            
                        </div>
                        </div>
                   <div class="col-md-4">
                <div class="form-group">
                            Mobile
                            
                            <asp:TextBox ID="txtmobile" class="form-control" runat="server" ></asp:TextBox>
                           
                        </div></div>

                  <div class="col-md-4">
                <div class="form-group">
                        
                           Designation

 <asp:TextBox ID="txtdesig" class="form-control" runat="server"></asp:TextBox>


                        </div></div>
                   

                        </div></div>
            

           
        </div>
       <br />
        <div class="control-group text-center" style="padding-top: 20px">
            <asp:Button ID="Add" runat="server" class="btn btn-success" OnClick="Add_Click"
                Text="Submit"></asp:Button>
            <%-- <asp:Button ID="Button4" runat="server"  Text="Not "></asp:Button>--%>
            <asp:Button ID="Button3" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>



              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div></div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>

    </section>



</asp:Content>
