﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;


namespace IIBFinal
{
    public partial class user : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Page.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        protected void BindStudy()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT Distinct dept FROM tblEmployee order by dept";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList2.DataSource = dt;

                        DropDownList2.DataTextField = "dept";
                        DropDownList2.DataBind();
                        sqlConn.Close();
                        DropDownList2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Department", "0"));
                    }
                }
            }
            catch { }
        }


        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT id,name FROM tblemployee where dept='" + DropDownList2.SelectedItem.Text + "' ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList1.DataSource = dt;
                        DropDownList1.DataValueField = "id";
                        DropDownList1.DataTextField = "name";

                        DropDownList1.DataBind();
                        sqlConn.Close();
                        DropDownList1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Employee", "0"));


                    }
                }
            }
            catch
            {

            }
        }
        //protected void txt_Mobilesearch(object sender, EventArgs e)
        //{
        //    string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;

        //    try
        //    {
        //        using (SqlConnection sqlConn = new SqlConnection(connstrg))
        //        {
        //            using (SqlCommand sqlCmd = new SqlCommand())
        //            {

        //                SqlCommand cmd = new SqlCommand("select top 1 name,Email,Company,Address,department,WhoomToMeet from Visitors where Mobile='" + txtmbl.Text + "' order by ID desc");
        //                cmd.CommandType = CommandType.Text;

        //                cmd.Connection = sqlConn;

        //                sqlConn.Open();
        //                SqlDataReader dr = cmd.ExecuteReader();
        //                if (dr.Read())
        //                {

        //                    txtvisitor.Text = dr["name"].ToString();
        //                    txtemail.Text = dr["Email"].ToString();
        //                    txtcompany.Text = dr["Company"].ToString();
        //                    txtaddress.Text = dr["Address"].ToString();
        //                    DropDownList2.SelectedItem.Text = dr["department"].ToString();
        //                    DropDownList1.SelectedItem.Text = dr["WhoomToMeet"].ToString();
        //                    //Double a = Convert.ToDouble(PNum.Text);
        //                    //QNum.Text = (a + 1).ToString();

        //                }
        //                else
        //                {
        //                    //   Name.Text = "Mr.";
        //                    // Email.Text = "";
        //                   txtcompany.Text = "";
        //                    txtaddress.Text = "";
        //                    DropDownList2.SelectedIndex = -1;
        //                    DropDownList1.SelectedIndex = -1;

        //                }

        //                dr.Close();

        //            }
        //        }
        //    }
        //    catch
        //    {

        //    }
        //}

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
                SqlConnection conn1 = new SqlConnection(connstrg);
                String insert = "insert into Visitors(Name,Mobile,Email,Address,Purpose,date,Department,WhoomToMeet,Company,statusby,asset,status,NoofPersons,floor,status1)values('" + txtvisitor.Text + "','" + txtmbl.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','" + Purpose.Text + "','" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "','" + DropDownList2.SelectedItem.Text + "','" + DropDownList1.SelectedItem.Text + "','" + txtcompany.Text + "','" + Session["id"].ToString() + "','" + txtassets.Text + "','arrive','" +txttotalvisitor.Text + "','" + lblfloor.Text + "','0')";
                SqlCommand comm1 = new SqlCommand(insert, conn1);

                conn1.Open();
                comm1.ExecuteNonQuery();
                Session["Mobile"] = txtmbl.Text;


                conn1.Close();
                clear();
                //string PATH = Server.MapPath("/Image/imagename.jpg");
                // Session["path"] = PATH;
                //  System.IO.File.Move(Server.MapPath("/Image/imagename.jpg"), Server.MapPath("/Image/" + vid + ".jpg"));
                Response.Redirect("UserList.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
                //Response.Redirect("default.aspx?");

            }

        }

        private void clear()
        {
            txtvisitor.Text = "";
            txtmbl.Text = "";
            txtemail.Text = "";
            txtaddress.Text = "";
            DropDownList2.SelectedIndex = -1;
            DropDownList1.SelectedIndex = -1;
            //WhoomToMeet.Text = "";
            Purpose.Text = "";
            txtdate.Text = "";
        }
    }
}