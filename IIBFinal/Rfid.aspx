﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="Rfid.aspx.cs"  Inherits="IIBFinal.Rfid" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>

    <section class="content-header">
      <h1>Date Wise Search</h1></section>
     <!----Heading End----> 


      <!-- Main content -->
    <section class="content">
	
	
	<div class="row">
     

         
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
             <h3>&nbsp</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                 
                  <div class="col-md-4">
                <div class="form-group">
                  <label>Start Date</label><span class="style1">*</span>
            
             
                    <asp:TextBox ID="TAN"   class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
          <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="TAN" Enabled="True" Format="MM/dd/yyyy"/>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TAN"
                              ErrorMessage="Enter Start Date" Style="color: #FF0000"></asp:RequiredFieldValidator>
                     
                </div>
                </div>
                   

                  <div class="col-md-4">
                <div class="form-group">
                  <label>End Date</label><span class="style1">*</span>
            
             
                 <asp:TextBox ID="TextBox1"   class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server"  TargetControlID="TextBox1" Enabled="True" Format="MM/dd/yyyy"/>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                              ErrorMessage="Enter Start Date" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                </div>
                  <div class="col-md-3" style=" padding-top:19px">
                    <asp:Button ID="Button1" class="btn btn-success btn-block" runat="server" Text="Search" onclick="Button1_Click"/>
       </div>

              
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div>
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3>&nbsp</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">

                       <asp:Label ID="search_results" runat="server" Text="" ></asp:Label>
                      <div class="col-md-12">
                   <div class="box-body table-responsive no-padding" style="overflow:scroll;height:450px">
               <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
           
            <%--<asp:ImageField DataImageUrlField="Photo" HeaderText="Picture" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
            </asp:ImageField>--%>
            <asp:TemplateField ItemStyle-Height = "150" ItemStyle-Width = "170" HeaderText="Picture">
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"
             ImageUrl = '<%# Eval("Photo", GetUrl("{0}")) %>' />
        </ItemTemplate>
    </asp:TemplateField>
           
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />

            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="date" HeaderText="Entry Time" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="outtime" HeaderText="Out Time" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="rfid" HeaderText="RFID No" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" Visible="false" />                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            

            
            
        </Columns>
    </asp:GridView>
<asp:Button ID="btn_Excel" runat="server" OnClick="btn_Excel_Click" Text="Export To Excel" Visible="false" />
                        </div>
 <asp:Label ID="lblCount" runat="server" Text="Label" Visible="false"></asp:Label>

              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div>


   
    </section>




</asp:Content>
