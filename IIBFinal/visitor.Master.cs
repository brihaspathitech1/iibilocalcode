﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIBFinal
{
    public partial class visitor : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["role"].ToString()=="hr1")
            {
                link4.Visible = false;
                linkk.Visible = false;
                linkl.Visible = false;
                link3.Visible = false;
                
            }
            else if (Session["role"].ToString() == "hr2")
            {
                link4.Visible = false;
                linkk.Visible = false;
                linkl.Visible = false;
                link3.Visible = false;

            }
            else if(Session["role"].ToString() == "ceo")
            {
                link1.Visible = false;
                link2.Visible = false;
                link3.Visible = false;
                link4.Visible = false;
                link5.Visible = false;
                link6.Visible = false;
                

            }
            else if(Session["role"].ToString()=="security")
            {
                link2.Visible = false;
                

                link1.Visible = false;
            }
        }
    }
}