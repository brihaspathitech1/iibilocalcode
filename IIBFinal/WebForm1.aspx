﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="IIBFinal.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>IIBI</title>
<link rel="stylesheet" href="badge/bootstrap.min.css">
<!-- jQuery library -->

<script src="Scripts/jquery-1.4.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="badge/bootstrap.min.js"></script>
<!-- jQuery library -->
<script src="Scripts/jquery-1.4.1.min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="badge/html5shiv.js"></script>
<script src="badge/respond.min.js"></script>
<![endif]-->
<style>
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
padding: 10px 0;
margin-left: 15px;
margin-right: 15px;
border-bottom: 3px solid #2fa3e6;
border-left: 0;
border-top: 0;
border-right: 0;
border-radius: 0;
}
p{ font-size:15px;
font-weight:bold}
h4{ font-size:35px !important;}
h5{ font-size:35px;
font-weight:bold}
page {
background: white;
display: block;
margin: 0 auto;
margin-bottom: 0.5cm;
box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
.print:last-child { page-break-after: auto; }
page[size="A4"] {
width: 21cm;
min-height:300px;
}
}
@media print {
body, page {
margin: 0;
box-shadow: 0;
font-size:24px !important
}	
page[size="A4"] {
width: 21cm;
min-height:300px;
}
/*.pagebreak {page-break-after: always;}*/
}
.hritem{margin-top: 0px;margin-bottom: 9px;border: 0; border-top: 1px solid #cecece;}
@media print {

.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
float: left;
}
.back{ visibility:hidden !important}
input{visibility:hidden}
.col-sm-12 {
width: 100%;
}
.col-sm-11 {
width: 91.66666666666666%;
}
.col-sm-10 {
width: 83.33333333333334%;
}
.col-sm-9 {
width: 75%;
}
.col-sm-8 {
width: 66.66666666666666%;
}
.col-sm-7 {
width: 58.333333333333336%;
}
.col-sm-6 {
width: 100%;
}
.col-sm-5 {
width: 41.66666666666667%;
}
.col-sm-4 {
width: 33.33333333333333%;
}
.col-sm-3 {
width: 25%;
}
.col-sm-2 {
width: 16.666666666666664%;
}
.col-sm-1 {
width: 8.333333333333332%;
}
.vendorListHeading{
background: #1a4567 !important;
color: white;}
.vendorListHeading th{
color:white;
}
.dcprint h4{font-size:38px !important}
.dcprint h5{font-size:18px !important}
}
img{width:100%}
.vendorListHeading{
background: #1a4567 !important;
color: white;
}
.vendorListHeading th{
color:white;
}

</style>
</head>
<body style="font-size:11.5px">
<form method="post" action="./Print1.aspx" id="form1">
 
<div class="text-center" style=" padding-top:5px">
<input type="button" id="btnPrint" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
<a href="VisitorsList.aspx" type="button" class="back" onclick="goBack()" ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>
</div>
<div>
<!--sidebar end-->
<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->
<!--main content start-->
<div class="container">
<div class="row " style="border:1px solid #cecece; padding:10px">
<div class="col-md-12 col-sm-12 print">
<div class="row">
<div class=" col-md-12 col-sm-12 text-center">
<img src="IIB  LOGO.png" class="text-center" style="width:190px;margin:auto;padding:0px;"/>
    <br />
    <br />
    
<%--<h3 class="text-center" style="font-size:44px !important;margin-top:-60px;"><b>Head Office, Hyderabad</b></h3>--%>
<h3 class="text-center"style="font-size:44px !important"><b>Visitors Slip</b></h3>

</div>

</div>

<hr>

<table width="100%">
<tr><td style="float:right"><h4><b>Date:</b>&nbsp
    <asp:Label ID="lblshortdate" runat="server" ></asp:Label></h4></td></tr>
</table>

 
<%--<div class="col-md-6 col-sm-6 text-center">
<h4><b>Floor</b> <asp:Label runat="server" Text="" ID="lblfloor" ></asp:Label> <b>Only</b></h4>
</div>--%>

 
<hr>
<div class="row ">
<div class=" col-md-9 col-sm-9">
<table id="DataList1" cellspacing="0" style="width:100%;border-collapse:collapse;">
<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Visitor No</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblvid" runat="server" Text="" ></asp:Label></h5>
</div>
</div>
</td>
</tr>
<tr>
<td>
 <div class="row">
<%--<div class="col-md-4 col-sm-4 ">
<h5><b>RFID No</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblRfid" runat="server" Text="" ></asp:Label></h5>
</div>--%>
</div>
</td>
</tr>

<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Name:</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblName" runat="server" Text=""></asp:Label></h5>
</div>

</div>
</td>
</tr>

<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Organization</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblCompany" runat="server" Text=""></asp:Label></h5>
</div>

</div>
</td>
</tr>
 

<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>To Meet</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label runat="server" Text="" ID ="lblcp" ></asp:Label></h5>
</div>

</div>
</td>
</tr>

 


<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Purpose</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label runat="server" Text="" ID ="lblpurpose" ></asp:Label></h5>
</div>

</div>
</td>
</tr>
<tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Sign In</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblDate" runat="server" Text="" ></asp:Label></h5>
</div>

</div>
</td>
</tr>
    <tr>
<td>
 <div class="row">
<div class="col-md-4 col-sm-4 ">
<h5><b>Sign Out</b>  </h5>
</div>
<div class="col-md-8 col-sm-8 ">
<h5><b>:</b> <asp:Label ID="lblsignout" runat="server" Text="" ></asp:Label></h5>
</div>

</div>
</td>
</tr>



<!---
<tr>
<td>
 <div class="row">
<div class="col-md-6 col-sm-6  ">
<h5><b>Assets of Visitor:</b> Bag, Helmet</h5>
</div>

<div class="col-md-6 col-sm-6 ">
<h5><b>Phone Number of Visitor: </b> 9989993242</h5>
</div>
</div>
</td>
</tr>
<tr>
<td>
 <div class="row">
<div class="col-md-6 col-sm-6  ">
<h5><b>Name Of officer to be contacted :</b> CB Sneha</h5>
</div>

<div class="col-md-6 col-sm-6 ">
<h5><b>Designation Of officer to be contacted: </b> Security Manager </h5>
</div>
</div>
<hr>
 <div class="row">
<div class="col-md-6 col-sm-6  text-center">
<h5><b>Signature of Officer</b></h5>
</div>

<div class="col-md-6 col-sm-6 text-center">
<h5> Signature of Receptionist</h5>
</div>
</div>
</td>
</tr>
--->

</table>
</div>
<div class=" col-md-3 col-sm-3 text-left">
 <asp:Image runat="server" style="width:300px;" class="img-responsive" id="imgvisitor" ></asp:Image>
<%--<img src="male-user-silhouette_318-35708.jpg" style="width:200px; margin:auto; padding:0px" class="img-responsive"/>--%>
<br>
<h5 style="font-size:25px !important"></h5>

</div>
</div>

 
<!--<div class="row">
 <hr>
 <div class="row">
<div class="col-md-6 col-sm-6  text-center">
<h3><b>Signature of Visitor</b></h3>
</div>

<div class="col-md-6 col-sm-6 text-center">
<h3><b> Signature of Officer</b></h3>
</div>
</div>-->

<br><br>
<table style="width:100%">
<tr>
<td width="50%" align="center"><h3><b>Signature of Visitor</b></h3></td>
<td width="50%" align="center"><h3><b>Signature of Officer </b></h3></td>
</tr>

</table>
 
  <div class="row">
<div class="col-md-12 col-sm-12">
<h2><b>Note:</b> <b> Please return the pass and visitor card to the security counter</b></h2>
</div>
 
</div>
<div class="row">
<div class="col-md-12 col-sm-12 text-center">
<h3><b>Thank You Visit Again</b></h3>
</div>
</div>
</div>
</div>
</div>
</form>
<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.8.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/jquery.sparkline.js"></script>
<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>
<script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="assets/js/gritter-conf.js"></script>
<!--script for this page-->
<script src="assets/js/sparkline-chart.js"></script>
<script src="assets/js/zabuto_calendar.js"></script>
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
</body>
</html>