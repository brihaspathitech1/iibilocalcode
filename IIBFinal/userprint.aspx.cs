﻿using System;
using System.Data;
using IIBI.DataAccess;

namespace IIBFinal
{
    public partial class userprint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string vid = Request.QueryString["id"].ToString();
                    DataSet ds = DataQueries.SelectCommon("select Name,company,date,WhoomToMeet,Photo,Purpose,signout from Visitors where vid='" + vid + "'");

                    lblName.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                    lblCompany.Text = ds.Tables[0].Rows[0]["company"].ToString();
                    lblvid.Text = vid;
                    //lblRfid.Text = ds.Tables[0].Rows[0]["rfid"].ToString();
                    lblDate.Text = ds.Tables[0].Rows[0]["date"].ToString(); /*ds.Tables[0].Rows[0]["date"].ToString();*/
                    //lblIntime.Text = date.ToShortTimeString();
                    lblcp.Text = ds.Tables[0].Rows[0]["WhoomToMeet"].ToString();
                    //lblfloor.Text = ds.Tables[0].Rows[0]["floor"].ToString();
                    lblpurpose.Text = ds.Tables[0].Rows[0]["Purpose"].ToString();
                    lblshortdate.Text = System.DateTime.Now.ToShortDateString();
                    lblsignout.Text = ds.Tables[0].Rows[0]["signout"].ToString();
                    imgvisitor.ImageUrl = "/Image/" + vid + ".jpg";


                    // string path = "/Image/qrcode.gif";
                    // GenerateQrcode(vid, path);
                    // barcode.ImageUrl = path;
                }
                catch { }
            }
        }

        private void GenerateBacode(string _data, string _filename)
        {
            //Linear barcode = new Linear();
            //barcode.Type = BarcodeType.CODE11;
            //barcode.Data = _data;
            //barcode.drawBarcode(_filename);
        }
        private void GenerateQrcode(string _data, string _filename)
        {
            //QRCode qrcode = new QRCode();
            //qrcode.Data = _data;
            //qrcode.DataMode = QRCodeDataMode.Byte;
            //qrcode.UOM = UnitOfMeasure.PIXEL;
            //qrcode.X = 3;
            //qrcode.LeftMargin = 0;
            //qrcode.RightMargin = 0;
            //qrcode.TopMargin = 0;
            //qrcode.BottomMargin = 0;
            //qrcode.Resolution = 72;
            //qrcode.Rotate = Rotate.Rotate0;
            //qrcode.ImageFormat = ImageFormat.Gif;
            //qrcode.drawBarcode(_filename);
        }
    }
}
