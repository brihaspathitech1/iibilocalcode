﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="imgupload.aspx.cs" Inherits="IIBFinal.imgupload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .style1 {
            color: #FF0000;
        }

        body {
            color: #000 !important;
            font-weight: bold !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>

    <section class="content-header">
      <h1>Add Visitor</h1></section>

    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

        


           <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
             <h3>&nbsp</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                 
                  <div class="col-md-4">
                <div class="form-group">
                   
                  <label>Visitor Name</label>
                     <span class="style1">*</span>
                    <asp:TextBox runat="server" ID="txtvisitor" class="form-control"></asp:TextBox>
                  
                </div>
                </div>
                  <div class="col-md-4">
                <div class="form-group">
                  
                  <label>Mobile</label>
                      <span class="style1">*</span>
                    <asp:TextBox runat="server" ID="txtmbl" class="form-control"></asp:TextBox>
                  
                </div>
                </div><div class="col-md-4">
                <div class="form-group">
                  <label>Email</label>
                    <asp:TextBox runat="server" ID="txtemail" class="form-control"></asp:TextBox>
                  
                </div>
                </div>
                  
                  <div class="col-md-4">
                <div class="form-group">
                   
                  <label>Date</label> <span class="style1">*</span>
                    <asp:TextBox runat="server" ID="txtdate" class="form-control" AutoComplete="off"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtdate" runat="server" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                  
                </div>
                </div>
                  
                  <div class="col-md-4">
                <div class="form-group">
                   
                    
                  <label>Company</label>
                     <span class="style1">*</span>
                    <asp:TextBox runat="server" ID="txtcompany" class="form-control"></asp:TextBox>
                  
                </div>
                </div><div class="col-md-4">
                <div class="form-group">
                  <label>Address</label>
                    <asp:TextBox runat="server" ID="txtaddress" class="form-control"></asp:TextBox>
                  
                </div>
                </div>
                  <div class="col-md-4">
                <div class="form-group">
                    
                  <label>Total Visitors</label>
                    <span class="style1">*</span>
                    <asp:TextBox runat="server" ID="txttotalvisitor" class="form-control"></asp:TextBox>
                  
                </div>
                </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Visitor's Assets</label>
                    <asp:TextBox runat="server" ID="txtassets" TextMode="MultiLine" class="form-control" Rows="2" placeholder="Enter the Assets Seperated by comma ' , '"></asp:TextBox>
                  
                </div>
                </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div>
         <!----Column End----> 

        <!----Panel End---->  



    </div>
    </section>

     <section class="content-header">
      <h1>Host deals</h1></section>

    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

        


           <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
             <h3>&nbsp</h3>
                
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>
             
                  <div class="col-md-4">
                <div class="form-group">
                    
                
                  
                  <div class="col-md-4">
                <div class="form-group">
                  <asp:Button ID="btnsave" runat="server" Text="Add" class="btn btn-success btn-block"  OnClientClick="return Capture();" />
                  
                </div>
                </div>
                  </div></div>

</div></div></div>


        <script>
                // This example displays an address form, using the autocomplete feature
                // of the Google Places API to help users fill in the information.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        { types: ['geocode'] });

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();

                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOSxb5hyc4b2X7YF9WZJVDVjsKhrEd1lI&libraries=places&callback=initAutocomplete"
                async defer></script>
    </section>
</asp:Content>
