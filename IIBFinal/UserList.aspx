﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="IIBFinal.UserList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <asp:ScriptManager runat="server">
    </asp:ScriptManager>
      <h1>Visitors List</h1></section>
    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

         <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
            <%--  <h3>&nbsp</h3>--%>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                <div class="col-md-1">
                    <h3 class="text-center" style="padding-top:3px; font-size:30px !important"><asp:LinkButton ID="btnFilter" runat="server"  class="fa fa-filter"  BorderWidth="0px" onclick="btnFilter_Click">
                  
                </asp:LinkButton></h3>
                 </div>
                  <div class="col-md-1">
                  <h3><asp:Button ID="btnToday" runat="server" Text="All users" 
                          class="btn btn-success" onclick="btnToday_Click"></asp:Button></h3>
                 </div>

             
                         <div class="col-md-1">
                 <h3><asp:Button ID="btnsignout" runat="server" Text="SignOut" 
                          class="btn btn-danger"   onclick="btnsignout_Click" Visible="false"></asp:Button></h3>
                 </div>
<asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                      <div class="col-md-12">
                   <div class="box-body table-responsive no-padding" style="overflow:scroll;height:450px">
<asp:GridView ID="GridView1" class="table table-bordered table-striped"  runat="server" DataKeyNames="Id" AutoGenerateColumns="False" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
           
       
            <asp:TemplateField>
        <ItemTemplate>
           <asp:Image ID="Image1" Height = "50" Width = "50" runat="server" 
             ImageUrl='<%# ResolveUrl(Eval("Photo").ToString()) %>'  onerror="this.src='default.png'"/>      </ItemTemplate>
   </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" />

            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" />
            <asp:BoundField DataField="Email" HeaderText="Email" />
            <asp:BoundField DataField="date" HeaderText="Entry Time" />
            <asp:BoundField DataField="Address" HeaderText="Address" />
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" />
            <asp:BoundField DataField="rfid" HeaderText="RFID No" Visible="false" />                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" />
            <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" />
             <asp:TemplateField HeaderText="Status" Visible="false">
                <ItemTemplate>
              <asp:LinkButton ID="lnkEdit"  OnClick="lnkEdit_Click" runat="server" Text="Status" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
          
            <asp:TemplateField HeaderText="Badge" Visible="false">
                <ItemTemplate>
               <asp:LinkButton ID="btnBadge"  OnClick="btnBadge_Click" runat="server" Text="Badge" Font-Size="Large" Visible="false"></asp:LinkButton>
                   </ItemTemplate>
            </asp:TemplateField>
           
             <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
              <asp:LinkButton ID="Edit" OnClick="Edit_Click" runat="server" Text="Edit" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>


<asp:Button ID="Button5" runat="server" style="display:none" />
   <cc1:ModalPopupExtender  ID="ModalPopupExtender1" runat="server" TargetControlID="Button5" PopupControlID="Panel1"
CancelControlID="Button3" BackgroundCssClass="tableBackground"></cc1:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" BackColor="Gray" Style="display: none; border: 1px solid #ceceec;
        padding-bottom: 20px">
        <div class="col-md-12">
            <h3 class="text-info">
                Update Status</h3>
            <hr />
            <div class="control-group text-center">
                <asp:DropDownList ID="ddstatus" runat="server" class="form-control">
                    <asp:ListItem>Select Status</asp:ListItem>
                    <asp:ListItem>Approved</asp:ListItem>
                    <asp:ListItem>Reject</asp:ListItem>
                    <asp:ListItem>Sign Off</asp:ListItem>
                    <asp:ListItem>Postponed to 5 min</asp:ListItem>
                    <asp:ListItem>Postponed to 15 min</asp:ListItem>
                    <asp:ListItem>Postponed to 30 min</asp:ListItem>
                    <asp:ListItem>Postponed to 1 hour</asp:ListItem>
                    <asp:ListItem>Postponed to 2 hour</asp:ListItem>
                    <asp:ListItem>Postponed to 4 hour</asp:ListItem>
                </asp:DropDownList>
             
            </div>
            <br />
             <div class="control-group text-center">
             <asp:TextBox ID="txtReason" runat="server" placeholder="Describe Reason..." class="form-control" TextMode="MultiLine"
                     ></asp:TextBox>
                 

       <%-- <cc1:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
            Enabled="True" Format="MM'/'dd'/'yyyy HH':'mm'" TargetControlID="txtpostpondatetime">
        </cc1:CalendarExtender>--%>
             </div>
        </div>
       <br />
        <div class="control-group text-center" style="padding-top: 20px">
            <asp:Button ID="Button2" runat="server" class="btn btn-success" OnClick="Button2_Click"
                Text="Submit"></asp:Button>
            <%-- <asp:Button ID="Button4" runat="server"  Text="Not "></asp:Button>--%>
            <asp:Button ID="Button3" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>
                         <asp:Button ID="ButtonEdit" runat="server" style="display:none" />            
   <cc1:ModalPopupExtender  ID="ModalPopupExtender2" runat="server" TargetControlID="ButtonEdit" PopupControlID="Panel2"
CancelControlID="Button3" BackgroundCssClass="tableBackground"></cc1:ModalPopupExtender>
  <asp:Panel ID="Panel2" runat="server" BackColor="Gray" Style="display: none; border: 1px solid #ceceec;
        padding-bottom: 40px">
         
                    <div class="col-sm-6">
                        <div class="col-sm-4">
                            <div class="col-sm-4 padbot">
                                <br />
                                Picture
                        

                        <object width="450" height="200">
                            <param name="movie1" value="WebcamResources/save_picture.swf" />
                            <embed src="WebcamResources/save_picture.swf" width="450" height="200" />
                        </object>


                            </div>

                        </div>

                    </div>
  
        <div class="control-group text-center" style="padding-bottom: 20px">
            <asp:Button ID="btnUpdate" runat="server" class="btn btn-success" OnClick="btnUpdate_Click"
                Text="Add" OnClientClick="return Capture();"></asp:Button>
            <%-- <asp:Button ID="Button4" runat="server"  Text="Not "></asp:Button>--%>
            <asp:Button ID="btnCancel" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>
 



              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div></div></section>

</asp:Content>
