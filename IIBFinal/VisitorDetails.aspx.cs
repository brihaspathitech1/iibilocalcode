﻿using System;
using System.Data;
using IIBI.DataAccess;

namespace IIBFinal
{
    public partial class VisitorDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString.Count == 0)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                string id = Request.QueryString["id"].ToString();
                string qry = "Select vid[Visitor ID],Name[Visitor Name],Mobile[Contact Number],Email,WhoomToMeet[Contact Person],floor[Floor],Purpose[Visit Purpose],";
                qry += " company[Visitor's Company],Status[Visit Status],statusreason[Description of Status],";
                qry += " (select name from AdminLogin where id=statusby)[Status Modified by],date[Signin at],signout[Signout at],Validity[Valid Upto],NoofPersons[Total Visitor] from Visitors where id='" + id + "'";

                DataSet ds = DataQueries.SelectCommon(qry);

                DataTable dt = new DataTable("tblVisitor");
                dt.Columns.Add("Prpoerty", typeof(string));
                dt.Columns.Add("Detail", typeof(string));

                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    DataRow row = dt.NewRow();
                    row["Prpoerty"] = ds.Tables[0].Columns[i].ColumnName;
                    row["Detail"] = ds.Tables[0].Rows[0][i].ToString().Trim();
                    dt.Rows.Add(row);
                }
                DataSet dataset = DataQueries.SelectCommon("select Photo,asset,waittime,vid from Visitors where Id='" + id + "'");

                String imagename = dataset.Tables[0].Rows[0]["Photo"].ToString();
                string vid = dataset.Tables[0].Rows[0]["vid"].ToString();
                string path = "/Image/" + vid + ".jpg";

                img.ImageUrl = path;
                                                                

                string waitime = dataset.Tables[0].Rows[0]["waittime"].ToString();
                if (waitime != "")
                {
                    string[] arr = waitime.Split('.');
                    if (arr.Length == 3)
                    {
                        waitime = "Day :" + arr[0] + "Time :" + arr[1];
                    }
                    else if (arr.Length == 2)
                    {
                        waitime = "Time :" + arr[0];
                    }

                    DataRow ro = dt.NewRow();
                    ro["Prpoerty"] = "Waiting Time";
                    ro["Detail"] = waitime;
                    dt.Rows.Add(ro);
                }
                string asset = dataset.Tables[0].Rows[0]["asset"].ToString();
                string[] assetarray = asset.Split(',');


                if (asset != "")
                {
                    for (int i = 0; i < assetarray.Length; i++)
                    {

                        if (i == 0)
                        {
                            DataRow row = dt.NewRow();
                            row["Prpoerty"] = "---Visitors Asset---";
                            row["Detail"] = assetarray.Length + " item";
                            dt.Rows.Add(row);
                        }

                        DataRow rw = dt.NewRow();
                        rw["Prpoerty"] = "Asset-" + (i + 1);
                        rw["Detail"] = assetarray[i].ToString().Trim();
                        dt.Rows.Add(rw); 
                    }
                }
                else
                {
                    DataRow row = dt.NewRow();
                    row["Prpoerty"] = "Visitors Asset";
                    row["Detail"] = "No item";
                    dt.Rows.Add(row);
                }


                gridVisitor.DataSource = dt;
                gridVisitor.DataBind();


            }

        }

    }
}