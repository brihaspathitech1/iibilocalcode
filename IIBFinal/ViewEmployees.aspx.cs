﻿using IIBI.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace IIBFinal
{
    public partial class ViewEmployees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Visitors();
        }



        private void Visitors()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from tblEmployee ", conn44);

            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

        protected void lnkupdate_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select * from tblEmployee where id='" + Label1.Text + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtempid.Text = ds.Tables[0].Rows[0]["id"].ToString();
                txtempname.Text = ds.Tables[0].Rows[0]["name"].ToString();
                txtdept.Text = ds.Tables[0].Rows[0]["dept"].ToString();
                txtdesig.Text = ds.Tables[0].Rows[0]["designation"].ToString();
                txtmobile.Text = ds.Tables[0].Rows[0]["mobile"].ToString();
                
            }


            this.ModalPopupExtender1.Show();


        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select name from tblEmployee where Id='" + Label1.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Delete = "delete from [tblEmployee]  where Id='" + Label1.Text + "'";
            SqlCommand comm = new SqlCommand(Delete, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("ViewEmployees.aspx");
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            DataSet ds = DataQueries.SelectCommon("select date from Visitors where Id='" + Label1.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Update = "update  [tblEmployee] set id='"+txtempid.Text+ "',name='" + txtempname.Text + "',dept='" + txtdept.Text + "',designation='" + txtdesig.Text + "',mobile='" + txtmobile.Text + "'  where Id='" + Label1.Text + "'";
            SqlCommand comm = new SqlCommand(Update, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("ViewEmployees.aspx");

        }
    }
}
