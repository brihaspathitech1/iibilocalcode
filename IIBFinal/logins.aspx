﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="logins.aspx.cs" Inherits="IIBFinal.logins" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
     <div class="row">
        <div class="col-md-12">
          <div class="box">
              
            <div class="box-header  with-border height-border">
              <h3 class="box-title"><b>logins List</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                
            </div>
             
            <!-- /.box-header -->
            <div class="box-body">
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped"  >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
            

               
                   
               <asp:BoundField DataField="name" HeaderText="name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="username" HeaderText="username" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="password" HeaderText="password" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
              
                  <asp:TemplateField HeaderText="update " HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit"   OnClick="lnkEdit_Click" Class="fa fa-pencil" 
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                
                       
           </Columns>
       </asp:GridView>
           </div>
    <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="300px" Width="500px"
                        Style="display: none" BorderStyle="Groove">

                                    <h3 style="text-align:center">Edit </h3>
                                <hr />
                       

                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                
                        
                          
                        
                             
                           
                           
                            
                             <div class="col-md-3">
                            <div class="form-group">
                              <b>name</b>
                                
                                    <asp:TextBox ID="nae" runat="server" class="form-control" />
                               
                                </div>
                                 </div>
                         
                             
                             <div class="col-md-3">
                            <div class="form-group">
                              <b>username</b>
                           
                               
                                    <asp:TextBox ID="userna" runat="server" class="form-control" />
                                
                                </div></div>
                               
                       
                         <div class="col-md-3">
                            <div class="form-group">
                               <b>Password</b> 
                                    <asp:TextBox ID="passw" runat="server" class="form-control"  />
                               
                                
                                </div>
                                 </div>
                       
                       
                       
                             
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                               
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" OnClick="btnUpdate_Click"
                                        Text="Update Data" class="btn btn-success" />
                                </div>

                                 </div>
                       
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                               </div>
                               
                            </div>
                    </asp:Panel>
                </div></div></div></div>
</asp:Content>


