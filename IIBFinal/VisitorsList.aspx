﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="VisitorsList.aspx.cs" Inherits="IIBFinal.VisitorsList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .zoom:hover {
 -ms-transform: scale(3.0); /* IE 9 */
 -webkit-transform: scale(3.0); /* Safari 3-8 */
 transform: scale(3.0); 
}
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <section class="content-header">
        <asp:ScriptManager runat="server">
    </asp:ScriptManager>
           <asp:UpdatePanel ID="UpdatePanel2" runat="server">
         <contenttemplate>
 
      <h1>Visitors List</h1></section>
    <section class="content">
	
	
	<div class="row"> 
     <!----Panel For Each Page---->  

         <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
            <%--  <h3>&nbsp</h3>--%>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                <div class="col-md-1">
                    <h3 class="text-center" style="padding-top:3px; font-size:30px !important"><asp:LinkButton ID="btnFilter" runat="server"  class="fa fa-filter"  BorderWidth="0px" onclick="btnFilter_Click">
                  
                </asp:LinkButton></h3>
                 </div>
                  <div class="col-md-1">
                  <h3><asp:Button ID="btnToday" runat="server" Text="All users" 
                          class="btn btn-success" onclick="btnToday_Click"></asp:Button></h3>
                 </div>

             
                         <div class="col-md-1">
                 <h3><asp:Button ID="btnsignout" runat="server" Text="SignOut" 
                          class="btn btn-danger"   onclick="btnsignout_Click" visible="false"></asp:Button></h3>
                 </div>

 <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                      <div class="col-md-12">
                   <div class="box-body table-responsive no-padding" style="overflow:scroll;height:450px">
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
            <%-- <asp:ImageField DataImageUrlField="Photo" HeaderText="Picture" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
            </asp:ImageField>--%>
            <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "150" ItemStyle-Width = "170" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("Photo", GetUrl("{0}")) %>' ControlStyle-CssClass = "zoom" ControlStyle-Height="100" ControlStyle-Width="100"/>
        </ItemTemplate>
    </asp:TemplateField>
                      
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />

            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="date" HeaderText="Entry Time" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="rfid" HeaderText="RFID No" Visible="false" />                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="outtime" HeaderText="OutTime" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
             <asp:TemplateField HeaderText="Status" Visible="false" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
              <asp:LinkButton ID="lnkEdit"  OnClick="lnkEdit_Click" runat="server" Text="Status" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Details" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
               <asp:LinkButton ID="btnDetails"  OnClick="btnDetails_Click" runat="server" Text="Details" Font-Size="Large"></asp:LinkButton>
                     </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Badge" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
               <asp:LinkButton ID="btnBadge"  OnClick="btnBadge_Click" runat="server" Text="Badge" Font-Size="Large"></asp:LinkButton>
                   </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Status" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
                <ItemTemplate>
              <asp:LinkButton ID="lnkupdate" OnClick="lnkupdate_Click" runat="server" Text="Exit" Font-Size="Large"></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

<asp:Button ID="Button5" runat="server" style="display:none" />
   <cc1:ModalPopupExtender  ID="ModalPopupExtender1" runat="server" TargetControlID="Button5" PopupControlID="Panel1"
CancelControlID="Button3" BackgroundCssClass="tableBackground"></cc1:ModalPopupExtender>


<asp:Panel ID="Panel1" runat="server" BackColor="Gray" Style="display: none; border: 1px solid #ceceec;
        padding-bottom: 20px">
        <div class="col-md-12">
            <h3 class="text-info">
                Update Status</h3>
            <hr />
            <div class="control-group text-center">
                <asp:DropDownList ID="ddstatus" runat="server" class="form-control">
                    <asp:ListItem>Select Status</asp:ListItem>
                    <asp:ListItem>Approved</asp:ListItem>
                    <asp:ListItem>Reject</asp:ListItem>
                    <asp:ListItem>Sign Off</asp:ListItem>
                    <asp:ListItem>Postponed to 5 min</asp:ListItem>
                    <asp:ListItem>Postponed to 15 min</asp:ListItem>
                    <asp:ListItem>Postponed to 30 min</asp:ListItem>
                    <asp:ListItem>Postponed to 1 hour</asp:ListItem>
                    <asp:ListItem>Postponed to 2 hour</asp:ListItem>
                    <asp:ListItem>Postponed to 4 hour</asp:ListItem>
                </asp:DropDownList>
             
            </div>
            <br />
             <div class="control-group text-center">
             <asp:TextBox ID="txtReason" runat="server" placeholder="Describe Reason..." class="form-control" TextMode="MultiLine"
                     ></asp:TextBox>
                 

      
             </div>
        </div>
       <br />
        <div class="control-group text-center" style="padding-top: 20px">
            <asp:Button ID="Button2" runat="server" class="btn btn-success" OnClick="Button2_Click"
                Text="Submit"></asp:Button>
          
            <asp:Button ID="Button3" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>
                        <asp:Button ID="btnupdate" runat="server" style="display:none" />
   <cc1:ModalPopupExtender  ID="ModalPopupExtender2" runat="server" TargetControlID="btnupdate" PopupControlID="Panel2"
CancelControlID="Button7" BackgroundCssClass="tableBackground"></cc1:ModalPopupExtender>

   <asp:Panel ID="Panel2" runat="server" BackColor="Gray" Style="display: none; border: 1px solid #ceceec;
        padding-bottom: 20px">
        <div class="col-md-12">
            <h3 class="text-info">
                Visitor OutTime</h3>
            <hr />
            
           
             <div class="control-group text-center">
             <asp:TextBox ID="txtupdate" runat="server" placeholder="eg: 00:00 AM" class="form-control"  AutoComplete="off"
                     ></asp:TextBox>
                 

             </div>
        </div>
       <br />
        <div class="control-group text-center" style="padding-top: 20px">
            <asp:Button ID="btnAdd" runat="server" class="btn btn-success" OnClick="btnAdd_Click"
                Text="Submit"></asp:Button>
            
            <asp:Button ID="Button7" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
        </div>
    </asp:Panel>
     </div>
                    </div>
                    </div>
                    

              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div></div>
         </contenttemplate>
    </asp:UpdatePanel>
    </section>









</asp:Content>
