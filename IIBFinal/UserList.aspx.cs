﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IIBI.DataAccess;


namespace IIBFinal
{
    public partial class UserList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Visitors();
            }
        }
        private void Visitors()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Visitors where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status1='0' order by id  desc", conn44);

            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();

        }

        protected void btnBadge_Click(object sender, EventArgs e)
        {


            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            string id = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("Select vid from Visitors where Id='" + id + "'");

            Response.Redirect("userprint.aspx?id=" + ds.Tables[0].Rows[0][0].ToString());

        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label1.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            //Label2.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");

            ModalPopupExtender2.Show();
        }

        protected void btnToday_Click(object sender, EventArgs e)
        {
            string date = System.DateTime.Now.ToString();
            DataSet ds = DataQueries.SelectCommon("select * from Visitors where status1='0' order by id desc");
            GridView1.DataSource = ds;
            GridView1.DataBind();
        }

        protected void btnsignout_Click(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Visitors where DATEPART(YEAR,date)= DATEPART(YEAR,GETDATE()) and DATEPART(MONTH,date)= DATEPART(MONTH,GETDATE()) and DATEPART(DAY,date)= DATEPART(DAY,GETDATE()) and status='Arrive' and status1='0' ", conn44);
           // cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = txtFilter.Text.Trim();
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = "";
            int n = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();

                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
           string key = "";
            string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("spgetlist", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter param = new SqlParameter("@key", key);
            cmd.Parameters.Add(param);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (ddstatus.SelectedItem.Value == "Select Status")
            {
                Response.Write("<script>alert('Please..! Select the Status')</script>");
                return;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                double vid = newvid();

              

                string connstrg = ConfigurationManager.ConnectionStrings["IIBI"].ConnectionString;
                SqlConnection conn1 = new SqlConnection(connstrg);
                String insert = "update Visitors set Photo='/Image/" + vid + ".jpg',vid='" + vid + "',status1='1' where Id='" + Label1.Text + "'";
                SqlCommand comm1 = new SqlCommand(insert, conn1);

                conn1.Open();
                comm1.ExecuteNonQuery();

                conn1.Close();

                Response.Redirect("WebForm1.aspx?id=" + vid);
            }
            catch (Exception ex)
            {
                throw ex;
                //Response.Redirect("default.aspx?");

            }
        }
        private double newvid()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(max(vid),0) from Visitors");
            double vid = 100;
            if (Convert.ToString(ds.Tables[0].Rows[0][0]) != "")
            {
                vid = double.Parse(ds.Tables[0].Rows[0][0].ToString());
                vid = vid + 1;
            }
            return vid;
        }

    }
}