﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="Searchvisitor.aspx.cs" Inherits="IIBFinal.Searchvisitor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <section class="content-header">
      <h1>Visitors list</h1></section>
    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

         <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
            <%--  <h3>&nbsp</h3>--%>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                
                   <div class="col-md-6" style="padding-top:15px">
                    <asp:TextBox ID="txtFilter" runat="server" class="form-control" 
                        placeholder="Search..." ontextchanged="txtFilter_TextChanged">
                   
                    </asp:TextBox>
                   
                    </div> 
                      
                 </div></div></div>  

              <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                         
 <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                      <div class="col-md-12">
                   <div class="box-body table-responsive no-padding" style="overflow:scroll;height:450px">
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
           
            <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "150" ItemStyle-Width = "170" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("Photo", GetUrl("{0}")) %>' ControlStyle-CssClass = "zoom" ControlStyle-Height="100" ControlStyle-Width="100"/>
        </ItemTemplate>
    </asp:TemplateField>

           
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px"/>
            <asp:BoundField DataField="date" HeaderText="Entry Time" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" />
            <asp:BoundField DataField="outtime" HeaderText="Out Time" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px"/>
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px"/>
            <asp:BoundField DataField="rfid" HeaderText="RFID No" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" Visible="false"/>                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px"/>
            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px"/>
            

            
            
        </Columns>
    </asp:GridView>




              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div></div></section>




</asp:Content>
