﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="VisitorDetails.aspx.cs" Inherits="IIBFinal.VisitorDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
.flr{ float:right !important;
      }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper" style="padding-top:35px; min-height:600px">
                <div class="col-md-12 panel" style="margin-top: 10px; padding-bottom: 15px">
                    <h4><a href="VisitorsList.aspx"><i class="fa fa-arrow-circle-left" style="font-size:26px;color:SteelBlue"></i></a>  Visitor Details</h4>
                    
                    <hr />
                    <div class="col-md-8">
                          <div class="table table-responsive" style="overflow-y:scroll;height:400px">
                            <asp:GridView ID="gridVisitor" runat="server" class="table table-bordered table-striped"></asp:GridView>
                         </div>
                    </div>
                     <div class="col-md-4 text-center">
                         <asp:Image ID="img" runat="server" class="img-responsive"  style="margin-top:auto; padding:0px;    width: 391px;
    padding-top: 43px;"></asp:Image>
                         </div>
              </div>
        </section>
    </section>
</asp:Content>
