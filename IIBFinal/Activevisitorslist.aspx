﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="Activevisitorslist.aspx.cs" Inherits="IIBFinal.Activevisitorslist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
        .tpadleft
        {
            padding-left: 20px;
        }
        .tpadright
        {
            padding-right: 20px;
        }
        .form-control1
        {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        body {
            color:#000 !important;
            font-weight:bold !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
         <asp:UpdatePanel ID="UpdatePanel2" runat="server">
         <contenttemplate>
 
            <div class="row mt">

 
                <div class="col-lg-12 panel" style="margin-top: -30px !important; min-height: 540px;">
                    <div class="col-md-12 panel" style="margin-top: 10px; padding-bottom: 15px">
                    <h4>Visitors List</h4>
               
                    
<br />
                         
                   
                    
                    </div>   
                    <div class="col-md-12">
                    <div class="box-body table-responsive no-padding" style="overflow:scroll;height:450px">
                    <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
           
          <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "150" ItemStyle-Width = "170" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("Photo", GetUrl("{0}")) %>' ControlStyle-CssClass = "zoom" ControlStyle-Height="100" ControlStyle-Width="100"/>
        </ItemTemplate>
    </asp:TemplateField>


           
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>

            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="date" HeaderText="Entry Time" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="rfid" HeaderText="RFID No" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" Visible="false"/>                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="asset" HeaderText="Assets" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            </Columns>
    </asp:GridView>

    </div>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>
                        </contenttemplate>
    </asp:UpdatePanel>
                    </section>
                    </section>
</asp:Content>
