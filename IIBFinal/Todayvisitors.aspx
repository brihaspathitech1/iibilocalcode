﻿<%@ Page Title="" Language="C#" MasterPageFile="~/visitor.Master" AutoEventWireup="true" CodeBehind="Todayvisitors.aspx.cs" Inherits="IIBFinal.Todayvisitors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <asp:ScriptManager runat="server">
    </asp:ScriptManager>
      <h1>Visitors List</h1></section>
    <section class="content">
	
	
	<div class="row">
     <!----Panel For Each Page---->  

         <!----Column For Each Page----> 
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
            <%--  <h3>&nbsp</h3>--%>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
               
 <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped" runat="server" DataKeyNames="Id" AutoGenerateColumns="False" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
           
            <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "150" ItemStyle-Width = "170" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px">
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("Photo", GetUrl("{0}")) %>' ControlStyle-CssClass = "zoom" ControlStyle-Height="100" ControlStyle-Width="100"/>
        </ItemTemplate>
    </asp:TemplateField>

           
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>

            <asp:BoundField DataField="Mobile" HeaderText="Contact Number" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" />
            <asp:BoundField DataField="date" HeaderText="Entry Time" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="outtime" HeaderText="Out Time" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="WhoomToMeet" HeaderText="Contact Person" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="rfid" HeaderText="RFID No" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" Visible="false"/>                          
            <asp:BoundField DataField="Purpose" HeaderText="Purpose" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"/>
            </Columns>
    </asp:GridView>



              </div>
              </div>
              <!-- /.row -->
            </div>
           
          </div>
          <!-- /.box -->
        </div></div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>

    </section>



</asp:Content>
